#!/bin/bash

echo -e "\n==>>   Tools installation   <<==\n"
sudo apt update && sudo apt install -y curl
echo -e "\n==>>   Adding MSSQL Server repository   <<==\n"
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
echo -e "\n==>>   Repository for MSSQL Server   <<==\n"
sudo add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/18.04/mssql-server-2019.list)"
echo -e "\n==>>   Repository for SQL Command Line tools   <<==\n"
curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list

echo ""; echo -e "\n==>>   MSSQL Server installation   <<==\n"
sudo apt update && sudo apt install -y mssql-server
echo -e "\n==>>   MSSQL Server configuration   <<==\n"
sudo /opt/mssql/bin/mssql-conf setup
echo -e "\n==>>   Command line tools installation   <<==\n"
sudo apt install -y mssql-tools

echo -e "\n==>>   Setting the PATH variable   <<==\n"
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
systemctl status mssql-server
