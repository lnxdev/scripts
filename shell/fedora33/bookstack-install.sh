#/usr/bin/bash

# Section: Session Variables

echo -e "\n==>>   Loading installation parameters   <<=="

# Root password for MariaDB
export DB_ROOT_PASS='somesecurepassword'
# Database name to use for application
export DB_NAME='bookstack'
# Database user to use for application
export DB_USER='bs_user'
# The domain name you have setup for the application
export APP_FQDN='http:\/\/jbod0.local.lnxorg'
# Folder to install application into
export APP_DIR='/var/www/html/bookstack'
# Generate a random password for the bookstack database user
export DB_PASS="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)"
#SELinux RW label for Apache
export HTTPDRW='httpd_sys_rw_content_t'

echo -e "\n==>>   Tools installation   <<==\n"
sudo dnf install -y nano mc wget unzip curl htop tree
echo -e "\n==>>   LAMP installation   <<==\n"
sudo dnf install -y composer git mariadb mariadb-server mcrypt nano php php-cli php-curl php-fpm php-gd php-json php-mbstring php-mysqlnd php-openssl php-pdo php-tidy php-tokenizer php-xml php-zip policycoreutils policycoreutils-python-utils

# Section: Firewall Settings

echo -e "\n==>>   Firewall configuration   <<==\n"
echo -e "Allow HTTP through the firewall default zone: \c"; sudo firewall-cmd --add-port=http/tcp --permanent
echo -e "Firewall reload: \c;"; sudo firewall-cmd --reload

# Section: Services Start

echo -e "\n==>>   Start MariaDB service   <<==\n"
sudo systemctl enable --now mariadb.service && systemctl status mariadb.service
echo -e "\n===>>   Start Apache service   <<==\n"
sudo systemctl enable --now httpd.service && echo "" && sudo netstat -tap | grep http && sleep 2

# Section: Database Configuration

echo -e "\n==>>   Create Database for BookStack   <<=="

# Create Database and user with a random password for Bookstack
sudo mysql -e "CREATE DATABASE $DB_NAME;"
sudo mysql -e "CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';"
sudo mysql -e "GRANT ALL ON $DB_NAME.* TO '$DB_USER'@'localhost';"
sudo mysql -e "FLUSH PRIVILEGES;"

echo -e "\n==>>   Secure MariaDB   <<=="
# Secure MariaDB (this does what mysql_secure_installation performs without interaction)
sudo mysql -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('$DB_ROOT_PASS');"
sudo mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
sudo mysql -e "DELETE FROM mysql.user WHERE User='';"
# sudo mysql -e "DROP DATABASE test;"
sudo mysql -e "FLUSH PRIVILEGES;"

# Section: BookStack Installation

echo -e "\n==>>   BookStack installation   <<==\n"
git clone https://github.com/ssddanbrown/BookStack.git --branch release --single-branch $APP_DIR

echo -e "\n==>>   Composer installation   <<==\n"
cd $APP_DIR; sudo composer install

# Section: SELinux

echo -e "\n==>>   SELinux Setup   <<=="
sudo setsebool -P httpd_can_sendmail 1
sudo setsebool -P httpd_can_network_connect 1
sudo semanage fcontext -a -t ${HTTPDRW} "${APP_DIR}/storage(/.*)?"
sudo restorecon -R -F ${APP_DIR}/storage
sudo semanage fcontext -a -t ${HTTPDRW} "${APP_DIR}/bootstrap/cache(/.*)?"
sudo restorecon -R -F ${APP_DIR}/bootstrap/cache
sudo semanage fcontext -a -t ${HTTPDRW} "${APP_DIR}/public/uploads(/.*)?"
sudo restorecon -R -F ${APP_DIR}/public/uploads

# Section: BookStack Environment

echo -e "\n==>>   BookStack Environment Setup   <<==\n"
# Create .env file and update variables
sudo cp $APP_DIR/.env.example $APP_DIR/.env
sudo sed -i "s/DB_DATABASE=.*\$/DB_DATABASE=$DB_NAME/" $APP_DIR/.env
sudo sed -i "s/DB_USERNAME=.*\$/DB_USERNAME=$DB_USER/" $APP_DIR/.env
sudo sed -i "s/DB_PASSWORD=.*\$/DB_PASSWORD=$DB_PASS/" $APP_DIR/.env
sudo sed -i "s/APP_URL=.*\$/APP_URL=$APP_FQDN/" $APP_DIR/.env

# Generate the application key
sudo php artisan key:generate --no-interaction --force
# Migrate the databases
sudo php artisan migrate --no-interaction --force

# Section: Apache Setup

echo -e "\n==>>   Apache vhost setup   <<=="

# Ensure ownership of the application directory is set to the web user (apache)
sudo chown apache: -R $APP_DIR
# Create tha Apache virtual host file
sudo cat > /etc/httpd/conf.d/bookstack.conf <<EOF
<VirtualHost *:80>
    <Directory $APP_DIR/public>
        Require all granted
        AllowOverride All
        #Options +Indexes
    </Directory>

    DocumentRoot $APP_DIR/public
    ErrorLog /var/log/httpd/bookstack.error.log
    CustomLog /var/log/httpd/access_log combined
</VirtualHost>
EOF

sudo systemctl restart httpd.service && echo "" && sudo netstat -tap | grep http

echo -e "\n==>>   BookStack Setup Finished   <<==\n"
echo -e "FQDN: \c" && echo $APP_FQDN
cat <<EOF

Username: admin@admin.com
Password: password

EOF
