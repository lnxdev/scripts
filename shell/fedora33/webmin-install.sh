#!/usr/bin/bash

echo -e "\n==>>   Adding Webmin repository   <<==\n"
cd /tmp/
cat > webmin.repo <<EOF
[Webmin]
name=Webmin Distribution Neutral
#baseurl=https://download.webmin.com/download/yum
mirrorlist=https://download.webmin.com/download/yum/mirrorlist
enabled=1
EOF
sudo mv webmin.repo /etc/yum.repos.d/
wget https://download.webmin.com/jcameron-key.asc
sudo rpm --import jcameron-key.asc

echo -e "\n==>>   Webmin installation   <<==\n"
sudo dnf install -y webmin net-tools perl
sudo netstat -pnltu | grep 10000
cat <<EOF

If the firewalld is enabled, then you need to open port 10000/TCP.
To do so, execute the command:

sudo firewall-cmd --add-port=10000/tcp --zone=public --permanent
sudo firewall-cmd --reload

EOF
sudo service webmin status
