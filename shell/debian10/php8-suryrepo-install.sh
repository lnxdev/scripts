#/usr/bin/bash

sudo apt install -y lsb-release apt-transport-https ca-certificates wget
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list

echo "==>>   PHP8 installation (for Apache)  <<=="
sudo apt update -y && sudo apt install -y php8.0

# echo "==>>   PHP8 installation (for Nginx)   <<=="
# sudo apt install -y php8.0-fpm

echo "==>>   PHP extensions   <<=="
sudo apt install -y php8.0-{mysql,cli,common,snmp,ldap,curl,mbstring,zip}
echo "==>>   Verify the PHP version   <<=="; php -v; php -m
cat <<EOF

To configure PHP 8 for the web applications like apache web server, 
edit its configuration file /etc/php/8.0/apache2/php.ini.

To configure php 8 fpm for NGINX web server, edit its configuration file 
/etc/php/8.0/fpm/pool.d/www.conf and set the parameters that suits to your
nginx server setup.

EOF
