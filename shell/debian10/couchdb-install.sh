#!/usr/bin/bash

echo -e "\n==>>   Adding CouchDB repository   <<==\n"
curl -L https://couchdb.apache.org/repo/bintray-pubkey.asc | sudo apt-key add -
sudo sh -c 'echo deb https://apache.bintray.com/couchdb-deb buster main > /etc/apt/sources.list.d/couchdb.list'

echo -e "\n==>>   CouchDB installation   <<==\n"
sudo apt update && sudo apt install couchdb net-tools -y

echo ""; sudo netstat -pnltu | grep 5984
echo ""; curl http://127.0.0.1:5984/
cat <<EOF

Open your browser and browse to http://127.0.0.1:5984/_utils/. 
Type in the admin username and password to login to your database.

EOF
echo ""; systemctl status couchdb.service
