#!/usr/bin/bash

echo -e "\n==>>   Tools installation   <<==\n"
sudo apt update && sudo apt install -y wget apt-transport-https software-properties-common
echo -e "\n==>>   Adding Webmin repository   <<==\n"
wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib"
echo -e "\n==>>   Webmin installation   <<==\n"
sudo apt install -y webmin
cat <<EOF

If the UFW firewall is enabled, then you need to open port 10000/TCP.
To do so, execute the command:

sudo ufw allow 10000/tcp
sudo ufw reload

EOF
systemctl status webmin.service
