#!/usr/bin/bash

VERSION="1.15.6"        # go version from https://golang.org/dl/
ARCH="amd64"            # Intel / AMD 64-bit processor
curl -O -L "https://golang.org/dl/go${VERSION}.linux-${ARCH}.tar.gz"
tar -xf "go${VERSION}.linux-${ARCH}.tar.gz"
sudo chown -R root: ./go
sudo mv -v go /usr/local
cat > ~/.bash_profile <<EOF

# set up Go lang path #
export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin

EOF
source ~/.bash_profile
go version

echo -e "\n==>>   Installation test   <<==\n"

cat > hello.go <<EOF
package main

import "fmt"

func main() {
	fmt.Printf("hello, world\n")
}
EOF
go run hello.go
cat <<EOF

If you see the "hello, world" message then Go is installed correctly.

EOF