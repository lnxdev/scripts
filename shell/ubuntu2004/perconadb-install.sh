#!/usr/bin/bash

echo -e "\n==>>   Adding Percona DB repository   <<==\n"
sudo apt update && apt install -y wget gnupg2
cd /tmp/ && wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
sudo dpkg -i percona-release_latest.focal_all.deb
echo "" && sleep 2 && sudo apt update

echo -e "\n==>>   Percona DB Server installation   <<==\n"
sudo percona-release setup ps80; echo ""
sudo apt install -y percona-server-server
echo ""; systemctl status mysqld.service
