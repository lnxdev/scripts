# scripts

## / shell /

| NAME | SYSTEM |
|------|--------|
| [PHP 8 on Debian 10](shell/debian10/php8-suryrepo-install.sh) | Debian 10 |
| [CouchDB on Debian 10](shell/debian10/couchdb-install.sh) | Debian 10 |
| [BookStack on Fedora 33](shell/fedora33/bookstack-install.sh) | Fedora 33 |
| [Webmin on Fedora 33](shell/fedora33/webmin-install.sh) | Fedora 33 |
| [MSSQL Server 2019 on Ubuntu 18.04](shell/ubuntu1804/mssql-install.sh) | Ubuntu 18.04 |
| [Go / Golang installation](shell/ubuntu2004/golang-install.sh) | Ubuntu 20.04 |
| [CouchDB on Ubuntu 20.04](shell/ubuntu2004/couchdb-install.sh) | Ubuntu 20.04 |
| [Webmin on Ubuntu 20.04](shell/ubuntu2004/webmin-install.sh) | Ubuntu 20.04 |
| [Percona Database Server on Ubuntu 20.04](shell/ubuntu2004/perconadb-install.sh) | Ubuntu 20.04 |  
